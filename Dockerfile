
FROM node:16

# Establece el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copia los archivos de tu aplicación al contenedor
COPY package.json package-lock.json /app/
COPY . /app/

# Instala las dependencias de la aplicación
RUN npm install

# Compila la aplicación 
RUN npm run build --prod
