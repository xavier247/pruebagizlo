import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; 

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TablaProductoComponent } from './tabla-producto/tabla-producto.component';
import { HttpClientModule } from '@angular/common/http';
import { FormularioProductoComponent } from './formulario-producto/formulario-producto.component';

@NgModule({
  declarations: [
    AppComponent,
    TablaProductoComponent,
    FormularioProductoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
