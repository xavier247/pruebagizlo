import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';


interface Producto {
  id: number;
  nombre: string;
  precio: number;
}

@Component({
  selector: 'app-formulario-producto',
  templateUrl: './formulario-producto.component.html',
  styleUrls: ['./formulario-producto.component.css']
})
export class FormularioProductoComponent {

  constructor(private http: HttpClient) { }

  nuevoPrd: Producto = {
    id: 0,
    nombre: "",
    precio: 0
  };

  agregarProducto() {
    // Aquí puedes enviar o guardar la nuevaPersona en tu backend o realizar otras acciones con los datos.
    console.log(this.nuevoPrd);
    // Limpia el formulario después de enviar los datos.

    this.http.post<Producto>('http://192.168.100.15:8080/productos', this.nuevoPrd).subscribe(
      (data) => {
        this.nuevoPrd = data;
      },
      (error) => {
        console.log('Error al obtener los Productos:', error);
        //this.resultado = error;
        ;
      }
    );

  }








}
