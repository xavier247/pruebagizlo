import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


interface Producto {
  id: number;
  nombre: string;
  precio: number;
}

@Component({
  selector: 'app-tabla-producto',
  templateUrl: './tabla-producto.component.html',
  styleUrls: ['./tabla-producto.component.css']
})
export class TablaProductoComponent implements OnInit {


  Productos : Producto[] = [];
  resultado : string="";

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.obtenerProductos();
  }

  obtenerProductos() {
    this.http.get<Producto[]>('http://192.168.100.15:8080/productos').subscribe(
      (data) => {
        this.Productos = data;
      },
      (error) => {
        console.log('Error al obtener los Productos:', error);
        this.resultado = error;
        ;
      }
    );
  }

  editarProducto(prd: Producto){

  }

  
  eliminarProducto( prd: Producto) {
    // Aquí puedes enviar o guardar la nuevaPersona en tu backend o realizar otras acciones con los datos.
    console.log(prd);
    // Limpia el formulario después de enviar los datos.

    this.http.post<Producto>('http://192.168.100.15:8080/productos/delete', prd).subscribe(
      (data) => {
       
      },
      (error) => {
        console.log('Error al obtener los Productos:', error);
        //this.resultado = error;
        ;
      }
    );

  }


 
}
